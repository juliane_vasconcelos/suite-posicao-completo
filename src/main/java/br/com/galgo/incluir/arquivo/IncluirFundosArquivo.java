package br.com.galgo.incluir.arquivo;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.galgo.incluir.Incluir;
import br.com.galgo.testes.recursos_comuns.enumerador.Canal;
import br.com.galgo.testes.recursos_comuns.enumerador.Grupo;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.config.MassaDados;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;
import br.com.galgo.testes.recursos_comuns.teste.TesteFundos;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesFundos;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;

public class IncluirFundosArquivo implements Incluir {

	private Teste teste;
	private Operacao operacao;
	private Ambiente ambiente;
	private final String PASTA_TESTE = "InclusaoFundoArquivo";

	@Before
	public void setUp() throws Exception {
		ambiente = TesteUtils
				.configurarTeste(Ambiente.HOMOLOGACAO, PASTA_TESTE);
		List<Teste> listaTeste = ArquivoUtils.getListaTeste(ambiente,
				ConstantesTestes.CAMINHO_TESTE_FUNDOS,//
				ConstantesFundos.ABA_CASOS_TESTE_FUNDOS,//
				ConstantesFundos.LINHA_INICIAL_TESTE_FUNDOS,//
				ConstantesFundos.COLUNA_GRUPO_FUNDOS,//
				ConstantesFundos.COLUNA_SERVICO_FUNDOS,//
				ConstantesFundos.COLUNA_CANAL_FUNDOS,//
				ConstantesFundos.COLUNA_ID_FUNDOS,//
				ConstantesFundos.COLUNA_OPERACAO_FUNDOS,//
				ConstantesFundos.COLUNA_DESCRICAO_FUNDOS,//
				ConstantesFundos.COLUNA_RETESTE_FUNDOS);
		Grupo grupo = Grupo.DEPOIS;
		Servico servico = null;
		operacao = Operacao.INCLUSAO_FUNDO;
		Canal canal = Canal.ARQUIVO;

		teste = Teste.fromListaTeste(listaTeste, ambiente, grupo, servico,
				operacao, canal);
	}

	@Test
	public void incluir() throws ErroAplicacao {
		incluir(teste);
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste("incluirFundos-depois.png");
	}

	public void incluir(Teste teste) {
		// TODO: ajustar arquivo com os valores da massa
		// enviar usuario da massa
		final String caminhoMassaDadosFundos = MassaDados.fromAmbiente(
				ambiente, ConstantesTestes.DESC_MASSA_DADOS_FUNDOS).getPath();
		TesteFundos testeFundos = new TesteFundos();
		testeFundos.configurar(
				ConstantesTestes.PATH_ARQUIVO_UPLOAD_LOTE_SUCESSO, operacao,
				ambiente);
		testeFundos.testeUploadFundos();
	}

}
