package br.com.galgo.incluir.arquivo;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.galgo.incluir.Incluir;
import br.com.galgo.testes.recursos_comuns.enumerador.Canal;
import br.com.galgo.testes.recursos_comuns.enumerador.Grupo;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesFundos;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;

public class IncluirCarteiraArquivo implements Incluir {

	Teste teste;

	@Before
	public void setUp() throws Exception {
		Ambiente ambiente = Ambiente.HOMOLOGACAO;
		List<Teste> listaTeste = ArquivoUtils.getListaTeste(ambiente,
				ConstantesTestes.CAMINHO_TESTE_FUNDOS,//
				ConstantesFundos.ABA_CASOS_TESTE_FUNDOS,//
				ConstantesFundos.LINHA_INICIAL_TESTE_FUNDOS,//
				ConstantesFundos.COLUNA_GRUPO_FUNDOS,//
				ConstantesFundos.COLUNA_SERVICO_FUNDOS,//
				ConstantesFundos.COLUNA_CANAL_FUNDOS,//
				ConstantesFundos.COLUNA_ID_FUNDOS,//
				ConstantesFundos.COLUNA_OPERACAO_FUNDOS,//
				ConstantesFundos.COLUNA_DESCRICAO_FUNDOS,//
				ConstantesFundos.COLUNA_RETESTE_FUNDOS);
		Grupo grupo = Grupo.DEPOIS;
		Servico servico = null;
		Operacao operacao = Operacao.INCLUSAO_CARTEIRA;
		Canal canal = Canal.ARQUIVO;

		teste = Teste.fromListaTeste(listaTeste, ambiente, grupo, servico,
				operacao, canal);
	}

	@Test
	public void incluir() throws ErroAplicacao {
		incluir(teste);
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste("incluirCarteira-depois.png");
	}

	public void incluir(Teste teste) {
		// TODO Auto-generated method stub

	}
}
