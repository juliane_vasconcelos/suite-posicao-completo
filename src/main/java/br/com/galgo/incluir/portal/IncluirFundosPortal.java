package br.com.galgo.incluir.portal;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.galgo.incluir.Incluir;
import br.com.galgo.testes.recursos_comuns.enumerador.Canal;
import br.com.galgo.testes.recursos_comuns.enumerador.Grupo;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.config.MassaDados;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtilsFundos;
import br.com.galgo.testes.recursos_comuns.file.entidades.Fundo;
import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaAmortizacao;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaContasFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaDatasFundos;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaDoctosAnexadosFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaEventos;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaIdentificacaoFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaInclusaoFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaParametrosMovimentacao;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaPerfilFundos;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaPrestadoresServicoFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaTaxasFundos;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesFundos;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;
import br.com.galgo.validacao.ValidarFundos;

public class IncluirFundosPortal implements Incluir {

	Teste teste;
	Operacao operacao = Operacao.INCLUSAO_FUNDO;
	Ambiente ambiente;
	private final String PASTA_TESTE = "InclusaoFundoPortal";

	@Before
	public void setUp() throws Exception {
		ambiente = TesteUtils.configurarTeste(Ambiente.PRODUCAO_NEW,
				PASTA_TESTE);
		List<Teste> listaTeste = ArquivoUtils.getListaTeste(ambiente,
				ConstantesTestes.CAMINHO_TESTE_FUNDOS,//
				ConstantesFundos.ABA_CASOS_TESTE_FUNDOS,//
				ConstantesFundos.LINHA_INICIAL_TESTE_FUNDOS,//
				ConstantesFundos.COLUNA_GRUPO_FUNDOS,//
				ConstantesFundos.COLUNA_SERVICO_FUNDOS,//
				ConstantesFundos.COLUNA_CANAL_FUNDOS,//
				ConstantesFundos.COLUNA_ID_FUNDOS,//
				ConstantesFundos.COLUNA_OPERACAO_FUNDOS,//
				ConstantesFundos.COLUNA_DESCRICAO_FUNDOS,//
				ConstantesFundos.COLUNA_RETESTE_FUNDOS);
		Grupo grupo = Grupo.ANTES;
		Servico servico = null;
		Canal canal = Canal.PORTAL;

		teste = Teste.fromListaTeste(listaTeste, ambiente, grupo, servico,
				operacao, canal);
	}

	@Test
	public void incluir() throws ErroAplicacao {
		incluir(teste);
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste("incluirFundos.png");
	}

	public void incluir(Teste teste) {
		final String caminhoMassaDadosFundos = MassaDados.fromAmbiente(
				ambiente, ConstantesTestes.DESC_MASSA_DADOS_FUNDOS).getPath();

		String login = ArquivoUtils.getLogin(teste, caminhoMassaDadosFundos);
		String senha = ArquivoUtils.getSenha(teste, caminhoMassaDadosFundos);
		Usuario usuario = new Usuario(login, senha, ambiente);

		String aba = ArquivoUtils.getAba(teste, caminhoMassaDadosFundos);

		List<Fundo> listaFundos = ArquivoUtilsFundos.getFundos(teste,
				caminhoMassaDadosFundos, aba, operacao);

		final String url = ambiente.getUrl();
		TelaGalgo.abrirBrowser(url);

		for (Fundo fundo : listaFundos) {
			incluir(usuario, fundo);
			ValidarFundos.validarInclusao(
					fundo.getIdentificaoFundo().getCnpj(), url, ambiente);
		}
	}

	private void incluir(Usuario usuario, Fundo fundo) {
		TelaLogin telaLogin = new TelaLogin();
		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaInclusaoFundo telaInclusaoFundos = (TelaInclusaoFundo) telaHome
				.acessarSubMenu(SubMenu.INCLUSAO_FUNDOS_INVESTIMENTO);

		TelaIdentificacaoFundo telaIdentificacaoFundo = telaInclusaoFundos
				.clicarBotaoIncluirFundo();
		TelaEventos telaEventos = telaIdentificacaoFundo.preencherAba(fundo
				.getIdentificaoFundo());
		TelaPerfilFundos telaPerfil = telaEventos.preencherAba(fundo
				.getEvento());
		TelaDatasFundos telaDatasFundos = telaPerfil.preencherAba(fundo
				.getPerfil());
		TelaPrestadoresServicoFundo telaPrestadoresServico = telaDatasFundos
				.preencherAba(fundo.getDatasFundos());
		TelaTaxasFundos telaTaxas = telaPrestadoresServico.preencherAba(fundo
				.getPrestadoresServico());
		TelaParametrosMovimentacao telaParametrosMovimentacao = telaTaxas
				.preencherAba(fundo.getTaxasFundos());
		TelaAmortizacao telaAmortizacao = telaParametrosMovimentacao
				.preencherAba(fundo.getParametrosMovimentacao());
		TelaContasFundo telaContas = telaAmortizacao.preencherAba(fundo
				.getAmortizacao());
		TelaDoctosAnexadosFundo telaDocumentosAnexados = telaContas
				.preencherAba(fundo.getContas());
		telaInclusaoFundos = telaDocumentosAnexados.preencherAba(
				fundo.getDoctosAnexados(), operacao);
		telaLogin.logout();
	}
}
