package br.com.galgo.validacao;

import org.junit.Assert;

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaValidacaoInclusaoFundos;

public class ValidarFundos {

	public static void validarInclusao(String cnpj, String url,
			Ambiente ambiente) {

		TelaLogin telaLogin = new TelaLogin();
		Usuario usuario = getUsuario(ambiente);
		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaValidacaoInclusaoFundos telaValidacaoInclusaoFundos = (TelaValidacaoInclusaoFundos) telaHome
				.acessarSubMenu(SubMenu.VALIDACAO_INCLUSAO_FUNDOS);
		telaValidacaoInclusaoFundos
				.clicarValidacaoInclusaoRegistro(telaValidacaoInclusaoFundos);
		telaValidacaoInclusaoFundos.verificaSegundaPagina();
		Assert.assertTrue("Fundo não incluído com sucesso",
				telaValidacaoInclusaoFundos.verificaTextoNaTela(cnpj));
		telaLogin.logout();

	}

	private static Usuario getUsuario(Ambiente ambiente) {
		Usuario usuario = null;
		if (Ambiente.HOMOLOGACAO == ambiente) {
			usuario = new Usuario(UsuarioConfig.AUTOREG_HOMOLOG);
		} else {
			if (Ambiente.PRODUCAO_NEW == ambiente) {
				usuario = new Usuario(UsuarioConfig.ANBIMA_NEW);
			} else {
				usuario = new Usuario(UsuarioConfig.ANBIMA);
			}
		}
		return usuario;
	}
}
